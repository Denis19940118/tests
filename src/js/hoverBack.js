const projects__container = document.querySelectorAll(
	'.projects__container-box-item'
);

projects__container.forEach(item => {
	item.addEventListener('mouseenter', handler);
	item.addEventListener('mouseleave', handlerOver);
});

function handler(event) {
	const element = document.createElement('div');
	element.classList.add('dark');
	element.innerHTML = `<div>
						<p class="dark__head">Досуговый \ центр</p>
						<button class="dark__btn"> детальнее  \ &rarr;</button>
					</div>`;
	const content = element;
	event.target.appendChild(content);
}

function handlerOver() {
	const element = document.querySelector('.dark');
	event.target.removeChild(element);
}
