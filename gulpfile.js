const gulp = require('gulp');
const sass = require('gulp-sass');
const fileinclude = require('gulp-file-include');
const clean = require('gulp-clean');
const cleanCss = require('gulp-clean-css');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync').create();
const minJs = require('gulp-js-minify');
const uglify = require('gulp-uglify');
const imagemin = require('gulp-imagemin');
const webphtml = require('gulp-webp-html');

const path = {
	dist: {
		html: 'dist',
		css: 'dist/css',
		js: './dist/js',
		img: 'dist/assets',
		ico: 'dist/assets/favicon',
		self: 'dist',
	},
	src: {
		html: 'src/*html',
		scss: 'src/scss/**/*.*',
		js: './src/js/**.js',
		img: 'src/assets/**/**/*.*',
		ico: 'src/assets/favicon/*.*',
	},
};
/*=============function================*/
function copyHtml() {
	return gulp
		.src(path.src.html)
		.pipe(fileinclude())
		.pipe(webphtml())
		.pipe(gulp.dest(path.dist.html))
		.pipe(browserSync.stream());
}
function copyScss() {
	return gulp
		.src(path.src.scss)
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer(['> 0.01%', 'last 100 versions']))
		.pipe(gulp.dest(path.dist.css))
		.pipe(browserSync.stream());
}

function copyJs() {
	return gulp
		.src(path.src.js)
		.pipe(concat('script.js'))
		.pipe(minJs())
		.pipe(uglify())
		.pipe(gulp.dest(path.dist.js))
		.pipe(browserSync.stream());
}

const assetsCopy = () =>
	gulp
		.src(path.src.img)
		// .pipe(imagemin())
		.pipe(gulp.dest(path.dist.img))
		.pipe(browserSync.stream());
function iconCopy() {
	return gulp
		.src(path.src.ico)
		.pipe(imagemin())
		.pipe(gulp.dest(path.dist.ico))
		.pipe(browserSync.stream());
}

const cleanProd = () =>
	gulp
		.src(path.dist.self, { allowEmpty: true })
		.pipe(clean())
		.pipe(cleanCss({ compatibility: 'ie8' }))
		.pipe(browserSync.stream());

const serve = () => {
	browserSync.init({
		server: {
			baseDir: './dist',
		},
	});
	gulp.watch(path.src.html, copyHtml).on('change', browserSync.reload);
	gulp.watch(path.src.scss, copyScss).on('change', browserSync.reload);
	gulp.watch(path.src.js, copyJs).on('change', browserSync.reload);
	gulp.watch(path.src.img, assetsCopy).on('change', browserSync.reload);
	gulp.watch(path.src.ico, iconCopy).on('change', browserSync.reload);
};

gulp.task(
	'default',
	gulp.series(
		cleanProd,
		copyHtml,
		copyScss,
		assetsCopy,
		copyJs,
		iconCopy,
		serve
	)
);
